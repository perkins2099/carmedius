Carbay::Application.routes.draw do
  require 'sidekiq/web'

  authenticate :admin_user do
    mount Sidekiq::Web => '/admin/sidekiq'
  end

  if Rails.env.development?
    mount RailsEmailPreview::Engine, :at => 'email_preview' # You can choose any URL here
  end

  resources :requests do
    resources :bids, only: [:new, :create, :index] 
  end

  resources :bids do
      member do
        get 'winner'
      end
  end

  resources :makes, only: [:index]
  resources :models, only: [:index] do
    member do
      get :years
    end
  end
  resources :cars, only: [:index] do
    collection do
      get 'style', action: :style, as: 'style'
    end
  end

  devise_for :users
  #ActiveAdmin.routes(self)

  resources :customers, only: [:update, :edit] do
  end

  get "dash" => "requests#index", as: 'dash'
  
  #root :to => 'high_voltage/pages#show', :id => 'home'
  get "/pages/*id" => 'pages#show', :as => :page, :format => false
  root :to => 'pages#show', :id => 'home'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

end
