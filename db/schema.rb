# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130507010632) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "bids", :force => true do |t|
    t.integer  "dealer_id"
    t.integer  "request_id"
    t.float    "amount"
    t.text     "other_offers"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.boolean  "won",          :default => false
  end

  add_index "bids", ["won"], :name => "index_bids_on_won"

  create_table "car_options", :force => true do |t|
    t.integer  "option_id"
    t.integer  "request_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "car_options", ["option_id"], :name => "index_car_options_on_option_id"
  add_index "car_options", ["request_id"], :name => "index_car_options_on_request_id"

  create_table "cars", :force => true do |t|
    t.integer  "make_id"
    t.integer  "model_id"
    t.integer  "year"
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "edmund_id"
  end

  add_index "cars", ["edmund_id"], :name => "index_cars_on_edmund_id"
  add_index "cars", ["make_id"], :name => "index_cars_on_make_id"
  add_index "cars", ["model_id"], :name => "index_cars_on_model_id"
  add_index "cars", ["year"], :name => "index_cars_on_year"

  create_table "customers", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address_line_1"
    t.string   "address_line_2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "phone_number"
  end

  create_table "dealer_makes", :force => true do |t|
    t.integer  "make_id"
    t.integer  "dealer_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "dealer_makes", ["dealer_id"], :name => "index_dealer_makes_on_dealer_id"
  add_index "dealer_makes", ["make_id"], :name => "index_dealer_makes_on_make_id"

  create_table "dealers", :force => true do |t|
    t.string   "contact_first_name"
    t.string   "contact_last_name"
    t.string   "contact_phone"
    t.string   "contact_fax"
    t.string   "contact_email"
    t.string   "email"
    t.string   "address_line_1"
    t.string   "address_line_2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.text     "website"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "makes", :force => true do |t|
    t.integer  "edmund_id"
    t.string   "name"
    t.string   "nice_name"
    t.text     "link"
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "makes", ["edmund_id"], :name => "index_makes_on_edmund_id"

  create_table "models", :force => true do |t|
    t.integer  "edmund_id"
    t.string   "link"
    t.string   "name"
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "make_id"
    t.string   "nice_name"
  end

  add_index "models", ["edmund_id"], :name => "index_models_on_edmund_id"
  add_index "models", ["make_id"], :name => "index_models_on_make_id"

  create_table "options", :force => true do |t|
    t.string   "name"
    t.integer  "car_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "options", ["car_id"], :name => "index_options_on_car_id"

  create_table "requests", :force => true do |t|
    t.integer  "make_id"
    t.integer  "model_id"
    t.integer  "year"
    t.text     "notes"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "car_id"
    t.integer  "customer_id"
    t.integer  "radius"
    t.string   "status",      :default => "Open"
    t.integer  "style_id"
  end

  add_index "requests", ["radius"], :name => "index_requests_on_radius"
  add_index "requests", ["status"], :name => "index_requests_on_status"
  add_index "requests", ["style_id"], :name => "index_requests_on_style_id"

  create_table "styles", :force => true do |t|
    t.integer  "car_id"
    t.string   "name"
    t.integer  "edmund_id"
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.text     "photo_data"
  end

  add_index "styles", ["car_id"], :name => "index_styles_on_make_id"
  add_index "styles", ["edmund_id"], :name => "index_styles_on_edmund_id"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "profileable_id"
    t.string   "profileable_type"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "admin",                  :default => false
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["profileable_id", "profileable_type"], :name => "index_users_on_profileable_id_and_profileable_type"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "years", :force => true do |t|
    t.integer  "model_id"
    t.integer  "year"
    t.boolean  "used"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "years", ["model_id"], :name => "index_years_on_model_id"
  add_index "years", ["used"], :name => "index_years_on_used"
  add_index "years", ["year"], :name => "index_years_on_year"

end
