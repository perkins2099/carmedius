class CreateDealers < ActiveRecord::Migration
  def change
    create_table :dealers do |t|
      t.string :contact_first_name
      t.string :contact_last_name
      t.string :contact_phone
      t.string :contact_fax
      t.string :contact_email
      t.string :email
      t.string :address_line_1
      t.string :address_line_2
      t.string :city
      t.string :state
      t.string :zip

      t.text :website

      t.timestamps
    end
  end
end
