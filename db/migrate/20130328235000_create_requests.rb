class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.integer :user_id
      t.integer :make_id
      t.integer :model_id
      t.integer :year
      t.integer :trim_id
      t.text :notes

      t.timestamps
    end
  end
end
