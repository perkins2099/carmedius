class AddWinnerToBid < ActiveRecord::Migration
  def change
    add_column :bids, :won, :boolean, default: "False"
    add_index :bids, :won
  end
end
