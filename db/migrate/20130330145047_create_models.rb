class CreateModels < ActiveRecord::Migration
  def change
    create_table :models do |t|
      t.integer :edmund_id
      t.string :link
      t.string :name
      t.text :years
      t.text :data

      t.timestamps
    end
  	add_index :models, :edmund_id
  	add_index :makes, :edmund_id
  end
end
