class CreateDealerMakes < ActiveRecord::Migration
  def change
    create_table :dealer_makes do |t|
      t.integer :make_id
      t.integer :dealer_id

      t.timestamps
    end
    add_index :dealer_makes, :make_id
    add_index :dealer_makes, :dealer_id
  end
end
