class AddEdmundIdToCars < ActiveRecord::Migration
  def change
  	add_column :cars, :style, :text
  	add_column :cars, :edmund_id, :integer
  	add_index :cars, :edmund_id
  end
end
