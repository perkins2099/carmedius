class RemoveUserIdRequests < ActiveRecord::Migration
  def up
    add_column :requests, :customer_id, :integer
    remove_column :requests, :user_id
  end

  def down
    add_column :requests, :user_id, :integer
  end
end
