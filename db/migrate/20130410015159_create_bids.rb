class CreateBids < ActiveRecord::Migration
  def change
    create_table :bids do |t|
      t.integer :dealer_id
      t.integer :request_id
      t.float :amount
      t.text :other_offers

      t.timestamps
    end
  end
end
