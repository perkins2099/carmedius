class Addbooleantorequest < ActiveRecord::Migration
  def change
    add_column :requests, :leather, :boolean
    add_column :requests, :navigation, :boolean
    add_column :requests, :premium_sound, :boolean
    add_column :requests, :backup_camera, :boolean
    add_column :requests, :reverse_sensor, :boolean
    add_column :requests, :heated_seats, :boolean
    add_column :requests, :sun_roof, :boolean
    add_column :requests, :awd, :boolean
    add_column :requests, :keyless_ignition, :boolean
    add_column :requests, :remote_start, :boolean
    add_column :requests, :bluetooth, :boolean
    add_column :requests, :tow_package, :boolean

    remove_column :requests, :features
  end

end
