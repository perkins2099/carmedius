class ChangeStyleToIntegerOnRequest < ActiveRecord::Migration
  def change
    remove_column :requests, :style
    add_column :requests, :style_id, :integer
    add_index :requests, :style_id
  end

end
