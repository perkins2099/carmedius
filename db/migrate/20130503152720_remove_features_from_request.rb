class RemoveFeaturesFromRequest < ActiveRecord::Migration
  def change
    remove_column :requests, :leather
    remove_column :requests, :navigation
    remove_column :requests, :premium_sound
    remove_column :requests, :backup_camera
    remove_column :requests, :reverse_sensor
    remove_column :requests, :heated_seats
    remove_column :requests, :sun_roof
    remove_column :requests, :awd
    remove_column :requests, :keyless_ignition
    remove_column :requests, :remote_start
    remove_column :requests, :bluetooth
    remove_column :requests, :tow_package
  end


end
