class AddMakeIdtoModel < ActiveRecord::Migration
  def up
  	add_column :models, :make_id, :integer
  	add_index :models, :make_id
  end

  def down
  end
end
