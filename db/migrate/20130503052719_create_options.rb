class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.string :name
      t.integer :car_id
      t.timestamps
    end
    add_index :options, :car_id
  end
end
