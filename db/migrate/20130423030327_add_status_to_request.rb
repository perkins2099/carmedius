class AddStatusToRequest < ActiveRecord::Migration
  def change
    add_column :requests, :status,:string, default: "Open"
    add_index :requests, :status
  end
end
