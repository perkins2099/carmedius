class AddRadiusToRequest < ActiveRecord::Migration
  def change
    add_column :requests, :radius, :integer
    add_index :requests, :radius
  end

end
