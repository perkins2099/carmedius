class CreateCarOptions < ActiveRecord::Migration
  def change
    create_table :car_options do |t|
      t.integer :option_id
      t.integer :request_id

      t.timestamps
    end
    add_index :car_options, :option_id
    add_index :car_options, :request_id
  end
end
