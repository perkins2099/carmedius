class RemoveTrimId < ActiveRecord::Migration
  def up
    remove_column :requests, :trim_id
  end

  def down
  end
end
