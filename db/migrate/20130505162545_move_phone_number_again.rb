class MovePhoneNumberAgain < ActiveRecord::Migration
  def change
    remove_column :users,:phone_number 
    add_column :customers,:phone_number, :string
  end

end
