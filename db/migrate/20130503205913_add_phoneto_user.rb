class AddPhonetoUser < ActiveRecord::Migration
  def change
    add_column :users, :phone_number, :string
    remove_column :customers, :primary_phone
  end


end
