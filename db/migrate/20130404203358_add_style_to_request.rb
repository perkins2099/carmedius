class AddStyleToRequest < ActiveRecord::Migration
  def change
    add_column :requests, :car_id, :integer
    add_column :requests, :style, :text
    add_column :requests, :features, :text
  end
end
