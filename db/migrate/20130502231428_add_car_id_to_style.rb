class AddCarIdToStyle < ActiveRecord::Migration
  def change
    rename_column :styles, :make_id, :car_id
  end
end
