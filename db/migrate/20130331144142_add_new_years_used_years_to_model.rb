class AddNewYearsUsedYearsToModel < ActiveRecord::Migration
  def change
    create_table :years do |t|
      t.integer :model_id
      t.integer :year
      t.boolean :used
      t.timestamps
    end
    add_index :years, :model_id
    add_index :years, :year
    add_index :years, :used
    remove_column :models, :years
  end
end
