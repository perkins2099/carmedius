class AddPhotoLinktoStyle < ActiveRecord::Migration
  def change
    add_column :styles, :photo_data, :text
  end

end
