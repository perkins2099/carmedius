class AddCarModel < ActiveRecord::Migration
  def change
    create_table :styles do |t|
      t.integer :make_id
      t.string :name
      t.integer :edmund_id
      t.text :data
      t.timestamps
    end

    create_table :cars do |t|
      t.integer :make_id
      t.integer :model_id
      t.integer :style_id
      t.integer :year

      t.text :data
      t.timestamps
    end

    add_index :cars, :make_id
    add_index :cars, :model_id
    add_index :cars, :style_id
    add_index :cars, :year

    add_index :styles, :make_id
    add_index :styles, :edmund_id

    add_column :models, :nice_name, :string
  end

end
