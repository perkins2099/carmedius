def running?
  running_processes = `ps -a | grep "[r]edis-server" | wc -l`
  return running_processes.strip.to_i >= 1
end

def stopped?
  !running?
end

namespace :redis do
  desc 'Start the Redis server in a screen session, if not running.'
  task :start do |cmd, args|
    if stopped?
      `screen -d -m redis-server`
    end
    Rake::Task['redis:status'].execute
  end

  desc 'Check whether the Redis server is running.'
  task :status do |cmd, args|
    puts running? ? "Redis is running!".green : "Redis is not running!".red
  end

  desc 'Stop the Redis server, if running.'
  task :stop do |cmd, args|
    if running?
      `redis-cli -p 6379 SHUTDOWN`
    end
    Rake::Task['redis:status'].execute
  end
end
