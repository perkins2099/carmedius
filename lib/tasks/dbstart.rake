namespace :db do
  task :start_postgres do
    puts "Starting Postgres... \n\n"
    sh "pg_ctl -w -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start" rescue RuntimeError
  end

  # task :start_solr do
  #   puts "\n\nStarting Solr in development... \n\n"
  #   sh "rake sunspot:solr:start RAILS_ENV=development" rescue RuntimeError

  #   puts "\n\nStarting Solr in test... \n\n"
  #   sh "rake sunspot:solr:start RAILS_ENV=test" rescue RuntimeError
  # end
  task :start_sidekiq do
    puts "\n\nStarting Sidekiq for dev... \n\n"
    sh "bundle exec sidekiq -q high -q medium -q default -q low" rescue RuntimeError
  end

  task :start_redis do
    puts "\n\nStarting Redis for dev... \n\n"
    sh "redis-server" rescue RuntimeError
  end


  desc "Start all necessary DB servers"
  task :start_all => [:start_postgres, :start_redis, :start_sidekiq] do
    puts "All databases started!"
  end

  desc "Stop all DB servers"
  task :stop_all do
    # sh "rake sunspot:solr:stop RAILS_ENV=development" rescue RuntimeError
    # sh "rake sunspot:solr:stop RAILS_ENV=test" rescue RuntimeError
    sh "pg_ctl -D /usr/local/var/postgres stop -s -m fast" rescue RuntimeError
    Rake::Task['redis:stop'].execute rescue RuntimeError
  end
end
