require 'spec_helper'

describe Edmund do

	describe 'update_makes' do
		it "should add only new records", :vcr, record: :new_episodes do
			Edmund.update_makes
			Make.count.should == 48
			Make.first.name.should == 'Acura'
			Make.first.edmund_id.should == 200002038
			Make.first.data.should_not be_nil
		end
	end  
	describe 'update_models' do
		it "should add only new records", :vcr, record: :new_episodes do
			make = FactoryGirl.create(:make)
			Edmund.update_models
			Model.count.should_not be_nil
			Model.first.name.should_not be_nil
			Model.first.id.should_not be_nil
			Model.first.data.should_not be_nil
			Model.first.years.should_not be_nil
			Model.first.make_id.should_not be_nil
		end
	end  
	describe 'update_cars' do
		it "should add only new records", :vcr, record: :new_episodes do
			make = FactoryGirl.create(:make)
			years = {"NEW"=>[2013]}
			model = FactoryGirl.create(:model, years: years)
			Edmund.update_cars
			Car.count.should == 1
			Car.first.style.should_not be_nil
			Car.first.data.should_not be_nil

		end
	end  
end
