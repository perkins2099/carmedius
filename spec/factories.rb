FactoryGirl.define do
  # factory :user do
  #   sequence(:username) { |n| "foo#{n}" }
  #   password "foobar"
  #   email { "#{username}@example.com" }
  #   admin false
    
  #   factory :admin do
  #     admin true
  #   end
  # end
  
  factory :make do
    name "Acura"
    nice_name "Acura"    
  end

  factory :model do
    name "ILX"
    nice_name "ILX"
    make
  end

  factory :style do
    name "4WD"
  end

  factory :year do
    year 2013
  end


end