class UserMailer < ActionMailer::Base
  default from: "support@carmedius.com"

  def new_bid(customer, bid)
    @user = customer.user
    @bid = bid
    mail(:to => @user.email, :subject => "New bid for your request - Carmedius.com")
  end

  class Preview
    def new_bid
      @customer = Customer.first
      @bid = Bid.first
      UserMailer.new_bid(@customer, @bid)
    end
  end

end
