class DealerMailer < ActionMailer::Base
  default from: "support@carmedius.com"

  def new_request(dealer, request)
    @dealer = dealer
    @request = request
    mail(:to => dealer.user.email, :subject => "New Bid Request - Carmedius.com")
  end

  def won_request(bid)
    @dealer = bid.dealer
    @request = bid.request
    @customer = @request.customer
    @bid = bid
    mail(:to => @dealer.user.email, :subject => "You WON a Request - Carmedius.com")
  end


  class Preview
    def new_request
      @dealer = Dealer.first
      @request = Request.first
      DealerMailer.new_request(@dealer, @request)
    end

    def won_request
      bid = Bid.where(won: true).first
      if !bid
        dealer = Dealer.first
        customer = Customer.first
        request = Request.first
        bid = Bid.first
        bid.won= true
        bid.save

      end
      DealerMailer.won_request(bid) 
    end
  end

end
