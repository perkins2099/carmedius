class StyleUpdateWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :low

  def perform(style_id)
    style = Style.find(style_id)
    style.update_from_edmund    
  end
end