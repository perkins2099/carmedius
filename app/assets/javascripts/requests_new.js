$(document).ready(function() {
  change_model_from_make();
  change_year_from_model();
  change_style_from_year();
  populate_initial_model();
  $('#request_option_ids').chosen();
});

function hide_spinner(){
  $("#spinner").hide();
} 
function show_spinner(){
  $("#spinner").show();
} 

function populate_initial_model(){
  update_model();
}

function change_model_from_make(){
  $("#request_make_id").change(function() {
    update_model();
  });
}

function change_year_from_model(){
  $("#request_model_id").change(function() {
    update_year();
  });
}

function change_style_from_year(){
  $("#request_year").change(function() {
    update_style();
  });
}

function update_model(){
  var make_id = $("#request_make_id").val();
  var $models = $("#request_model_id");
  var $styles = $("#request_style_id");
  $models.empty();
  $styles.empty();

  $.getJSON("/models.json?type=new&make_id="+make_id, function(data) {
    models = data
    $.each(data, function(key, value) {
        $('#request_model_id').append($("<option/>", {
            value: value.id,
            text: value.name
        }));
    });
    update_year();
  });
}

function update_style(){
  //var $dropdown = $(thiss);
  var make_id = $("#request_make_id").val();
  var model_id = $("#request_model_id").val();
  var year = $("#request_year").val();
  var $styles = $("#request_style_id");
  $styles.empty();
  show_spinner();
  $.getJSON("/cars/style.json?make_id="+make_id+"&model_id="+model_id+"&year=" + year, function(data) {
  
    models = data
    $.each(data, function(key, value) {
        $('#request_style_id').append($("<option/>", {
            value: value.id,
            text: value.name
        }));
      });
    hide_spinner();
    });

}

function update_year(){
  //var $dropdown = $(thiss);
  var make_id = $("#request_make_id").val();
  var model_id = $("#request_model_id").val();
  var $years = $("#request_year");
  $years.empty();
  $.getJSON("/models/" + model_id + "/years.json?&type=new", function(data) {
  
    models = data
    $.each(data, function(key, value) {
        $('#request_year').append($("<option/>", {
            value: value.year,
            text: value.year
        }));
      });
    update_style();
    });

}