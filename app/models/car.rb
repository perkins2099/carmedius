class Car < ActiveRecord::Base

	belongs_to :make
	belongs_to :model
  has_many :styles, dependent: :destroy

  has_many :years, through: :model
  serialize :data, Hash


  #scope :new_cars, joins(:models).joins(:years).where("cars.year = ?, and used = false", self.year)
  #scope :new_years, joins(:models),joins(:years).where(used: false)

	def name
		self.year.to_s + ' - ' + self.data["makeName"] + ' - '+ self.data["modelName"]
	end

  def self.locate(year, make_id, model_id)
    car = Car.where(year: year, make_id: make_id, model_id:model_id).first
    if !car
      car = Car.create_car_from_edmund(make_id, model_id, year)
    end
    car
  end

  def self.create_car_from_edmund(make_id, model_id, year)
    model = Model.find(model_id)
    make = Make.find(make_id)
    puts year.to_s + ' | ' + model.nice_name + ' | ' + make.nice_name   
    q = "http://api.edmunds.com/v1/api/vehicle/modelyearrepository/foryearmakemodel?make=" + make.nice_name + "&model=" + model.nice_name + "&year=" + year.to_s+ "&api_key=" + ENV['EDMUNDS_VEHICLE_KEY'] + "&fmt=json"
    response = Edmund.yank(q)
    car_json = JSON.parse(response.body)['modelYearHolder'][0]
    car = Car.new
    if car_json
      car.edmund_id = car_json["id"]
      car.make_id = make.id
      car.model_id = model.id
      car.year = year
      car.data = car_json
      car.save
      car.update_car_styles
    end
    if car.persisted?
      car
    else
      nil
    end
    #car if car.persisted?
  end

  def update_car_from_edmund
    puts year.to_s + ' | ' + self.model.nice_name + ' | ' + self.make.nice_name   
    q = "http://api.edmunds.com/v1/api/vehicle/modelyearrepository/foryearmakemodel?make=" + self.make.nice_name + "&model=" + self.model.nice_name + "&year=" + year.to_s+ "&api_key=" + ENV['EDMUNDS_VEHICLE_KEY'] + "&fmt=json"
    response = Edmund.yank(q)
    car_json = JSON.parse(response.body)['modelYearHolder'][0]
    car = Car.new
    if car_json
      car.data = car_json
      car.save
      car.update_car_styles
    end
  end


  def update_car_styles
    #Load the styles into proper records
    self.data["styles"].each do |s|
      style = self.styles.where(name: s["id"]).first
      if !style
        Style.create(car_id: self.id, data: s, name: s["name"], edmund_id: s["id"] )
      elsif s["name"] != style.name or style.data != s
        style.data = s
        style.name = s["name"]
        style.save
      end
    end   
  end

end
