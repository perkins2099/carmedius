class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, 
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body

  belongs_to :profileable, :polymorphic => true

  #Turn a customer into a dealer
  def make_dealer
    Customer.find(self.profileable_id).delete
    dealer = Dealer.create()
    self.profileable = dealer
    self.save
  end

  def customer?
    self.profileable.customer?
  end
  def dealer?
    self.profileable.dealer?
  end

  def generate_confirmation_token
    if self.profileable.blank?
      customer = Customer.new()
      customer.save(validate: false)
      self.profileable = customer
    end

    super # includes a call to save(validate: false), 
        # so be sure to call whatever you like beforehand
  end

end
