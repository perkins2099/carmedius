class Bid < ActiveRecord::Base
  attr_accessible :amount, :dealer_id, :other_offers, :request_id
  belongs_to :request
  belongs_to :dealer

  after_create :after_create

  validates :amount, :numericality => { :only_integer => true, message: "must be an integer, format it like this 19900, instead of $19,0000" }

  def after_create
    #Mail to the user
    UserMailer.new_bid(self.request.customer, self).deliver
  end

end
