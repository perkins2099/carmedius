class Edmund < ActiveRecord::Base
	

	def self.update_makes
		q = "http://api.edmunds.com/v1/api/vehicle-directory-ajax/findmakes?api_key=" + ENV['EDMUNDS_VEHICLE_KEY'] + "&fmt=json"
		response = yank(q)
		makes = JSON.parse(response.body)['makes']

		makes.each_with_index do |make_json, index|
			make_json = make_json[1]
			make = Make.find_by_edmund_id(make_json['id'])
			if make.blank?
				make = Make.new()
				make.edmund_id = make_json['id']
				make.name = make_json['name']
				make.nice_name = make_json['niceName']
				make.link = make_json['link']
				make.data = make_json
				make.save
			end
		end
	end

	def self.update_models()
		#Make.limit(3).each do |make|
		Make.order(:id).each do |make|
			begin
				q = "http://api.edmunds.com/v1/api/vehicle-directory-ajax/findmakemodels?make=" + make.nice_name + "&api_key=" + ENV['EDMUNDS_VEHICLE_KEY'] + "&fmt=json"
				response = yank(q)
				model_json = JSON.parse(response.body)['models']
				model_json.each do |m|
					m = m[1]
					model = Model.where(edmund_id: m['id'].to_i, make_id: make.id).first			
					if model.blank?
						model = Model.new()
					end
					if model.data != m or model.years.count == 0
						model.make_id = make.id
						model.edmund_id = m['id']					
						model.link = m['link']
						model.name = m['name']
						model.nice_name = m['model']
						model.data = m
						model.save
						Year.where(model_id: model.id).destroy_all
						if m["years"]["NEW"].present?
							m["years"]["NEW"].each do |x| 
								model.years.create(year: x, used: false)
							end
						end
						if m["years"]["USED"].present?
							m["years"]["USED"].each do |x| 
								model.years.create(year: x, used: true)
							end
						end
					end
				end
			rescue => ex
				puts ex.message
			ensure
				sleep 0.5
			end
		end
	end

	def self.yank(q)
		response = HTTParty.get(q)
		response
	end
end



