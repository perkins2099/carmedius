class Model < ActiveRecord::Base
  attr_accessible :edmund_id, :link, :name, :nice_name

  serialize :data, Hash

  belongs_to :make
  has_many :cars, dependent: :destroy
  has_many :years
  has_many :requests

  scope :new_models, where("models.id IN (SELECT years.model_id FROM years where years.used = false)").order(:name)
  scope :used_models, where("models.id IN (SELECT years.model_id FROM years where years.used = true)").order(:name)

  # scope :by_age, lambda do |age|
  #   joins(:profile).where('profile.age = ?', age) unless age.nil?
  # end
  # scope :by_name, lambda{ |name| where(name: name) unless name.nil? }
  # scope :new_years, includes(:)


  def newest_year
    self.years.where(used: false).map(&:year).sort.last
  end

  def new_years
  	self.years.where(used: false).map(&:year)
  end

  def used_years
    self.years.where(used: true).map(&:year)
  end

end
