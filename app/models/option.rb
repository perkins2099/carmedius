class Option < ActiveRecord::Base
  attr_accessible :name

  has_many :car_options
  has_many :requests, through: :car_options

  def self.seed
    Option.create(name: "Leather")
    Option.create(name: "Navigation")
    Option.create(name: "Premium Sound")
    Option.create(name: "Backup Camera")
    Option.create(name: "Reverse Sensor")
    Option.create(name: "Heated Seats")
    Option.create(name: "Sun Roof")
    Option.create(name: "AWD / 4WD")
    Option.create(name: "Keyless Ignition")
    Option.create(name: "Remote Start")
    Option.create(name: "Bluetooth")
    Option.create(name: "Tow Package")
  end
end
