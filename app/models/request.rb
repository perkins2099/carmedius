class Request < ActiveRecord::Base
  attr_accessible :make_id, :model_id, :notes, :style, :user_id, :year, 
    :leather,   :navigation,     :premium_sound,     :backup_camera,    :reverse_sensor,
    :heated_seats,    :sun_roof,   :awd,    :keyless_ignition,    :remote_start,
    :bluetooth,    :tow_package, :style_id, :option_ids

  default_scope order('requests.created_at DESC')

  belongs_to :customer
  belongs_to :make
  belongs_to :model
  has_many :available_dealers, through: :make, source: "dealers"
  belongs_to :style
  
  has_many :car_options
  has_many :options, through: :car_options

  has_one :winning_bid, class_name:'Bid', conditions:  "bids.won = 'true'"

  has_many :bids
  has_many :unique_bids, class_name: 'Bid', :finder_sql => proc { "SELECT bids.* FROM bids where request_id = #{id} and bids.id in (select max(bids2.id) from bids bids2 where bids2.request_id = bids.request_id group by bids2.dealer_id)" }

  #scope :winner, includes(:bid).where(won: true).limit(1)

  after_create :after_create

  self.per_page = 5

  def name
    self.year.to_s + ' ' + self.make.name + ' ' + self.model.name 
  end

  def after_create
    #Mail to all dealers
    dealers = self.available_dealers
    dealers.each do |dealer|
      DealerMailer.new_request(dealer, self).deliver
    end
  end

  def not_won?
    won_bids = self.bids.where(won: true)
    won_bids.blank?
  end

  def won?
    won_bids = self.bids.where(won: true)
    won_bids.present?
  end

  def photo
    self.style.photo
  end

  def get_options
    self.options.map(&:name).join(', ')
  end

  def has_new_bids
    if self.bids.present?
      return self.bids.order("created_at desc").first.created_at > (Time.now - 1.day)
    else
      false
    end
  end

  def did_i_bid_on_it?(user)
    if self.bids.present?
      "Yes" if self.bids.where(dealer_id:user.profileable.id).count > 0
    else
      "No"
    end
  end


end


