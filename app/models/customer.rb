class Customer < ActiveRecord::Base
  has_one :user, :as => :profileable
  has_many :requests
  attr_accessible :last_name,:first_name,:phone_number

  validates_presence_of :phone_number
  validates :phone_number, :length => { :is => 10 }
  validates_format_of :phone_number,
      :with => /[0-9]{3}[0-9]{3}[0-9]{4}/,
      :message => "- Phone numbers must be in xxxxxxxxxx format."

  validates_presence_of :first_name
  validates_presence_of :last_name

  def email
    self.user.email
  end
  def customer?
    true
  end
  def dealer?
    false
  end

  def full_name
    name = first_name 
    name = name + ' ' + last_name if last_name
    name
  end

end
