class CarOption < ActiveRecord::Base
  attr_accessible :option_id, :request_id

  belongs_to :request
  belongs_to :option
end
