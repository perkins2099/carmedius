class Dealer < ActiveRecord::Base
	has_one :user, :as => :profileable
  # attr_accessible :title, :body

  has_many :dealer_makes, dependent: :destroy
  has_many :makes, through: :dealer_makes
  has_many :bids

  has_many :available_requests, through: :makes, source: "requests"#, conditions:  "requests.status = 'Open'"
  has_many :requests, through: :bids, group: "requests.id"

  def customer?
    false
  end
  def dealer?
    true
  end

  def all_makes
    Make.all.each do |m|
      DealerMake.create(dealer_id: self.id, make_id: m.id)
    end
  end
end
