class Make < ActiveRecord::Base
  attr_accessible :edmund_id, :name, :data

  serialize :data, Hash

  has_many :models, dependent: :destroy
  has_many :requests

  has_many :dealer_makes, dependent: :destroy
  has_many :dealers, through: :dealer_makes


  scope :new_makes, where("makes.id in (SELECT models.make_id from models where models.id IN (SELECT years.model_id FROM years where years.used = false))").order(:name)
  scope :used_makes, where("makes.id in (SELECT models.make_id from models where models.id IN (SELECT years.model_id FROM years where years.used = true))").order(:name)


end
