class DealerMake < ActiveRecord::Base
  attr_accessible :dealer_id, :make_id

  belongs_to :dealer
  belongs_to :make
end
