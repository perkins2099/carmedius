class Year < ActiveRecord::Base
  attr_accessible :year, :used

  belongs_to :model
  has_many :cars, through: :model

  default_scope order("years.year desc")

end