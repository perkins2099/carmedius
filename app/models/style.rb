class Style < ActiveRecord::Base
  attr_accessible :data, :edmund_id, :link, :car_id, :name

  belongs_to :car
  has_many :requests

  serialize :data, Hash
  serialize :photo_data, Hash

  after_create :worker_update_style

  default_scope order("styles.name asc")

  def photo(shot = nil, res = nil)
  #["interior_CC", "interior_SHF", "exterior_W", "interior_G", "exterior_FBDG", "exterior_FQ", "exterior_R", "exterior_E", "interior_I", "exterior_S", "exterior_RQ", "interior_D", "exterior_EDETAIL", "exterior_RBDG", "interior_SWD", "interior_RI", "interior_DETAIL", "exterior_F"]
  #exterior_FBDG - Exterior Grill
  #exterior_W - Exterior Wheels
  #exterior_FQ - 2/3 elevated exterior shot - Front Quarter
  #exterior_R - Exterior Rear
  #exterior_E - Engine
  #exterior_S - Side
  #exterior_RQ - Rear Quarter
    if self.photo_data.blank?
      worker_update_style
      'default_car.jpg'
    else
      Style.edmund_path + self.photo_data["exterior_FQ"]["photoSrcs"].detect {|s| s.split('_')[-1].split('.')[0] == "300"}
    end
  end


  def self.edmund_path
    'http://media.ed.edmunds-media.com/'
  end

  def worker_update_style
    StyleUpdateWorker.perform_in(2.seconds, self.id)
  end

  def update_from_edmund
    q = "http://api.edmunds.com/v1/api/vehiclephoto/service/findphotosbystyleid?styleId=" + self.edmund_id.to_s + "&api_key=" + ENV['EDMUNDS_VEHICLE_KEY'] + "&fmt=json"
    response = Edmund.yank(q)
    if response.header.code != "200"
      puts response.to_yaml
      raise "Bad header code:  " + response.header.code
      return false
    end
    self.photo_data = {}
    JSON.parse(response.body).each do |p|
      self.photo_data[p["subType"]+'_'+p["shotTypeAbbreviation"]] = p
    end
    if self.photo_data.blank?
      raise "No Data - Canceling"
      return false
    else
      self.save
    end
  end

end
