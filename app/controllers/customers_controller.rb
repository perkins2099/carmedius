class CustomersController < ApplicationController
  layout 'no_header'
  before_filter :authenticate_user!
  def update
    @customer = Customer.find(params[:id])
    @request = Request.find(params[:request_id])

    respond_to do |format|
      if @customer.update_attributes(params[:customer])
        format.html { redirect_to @request, notice: 'Your contact information has been updated!  Thanks!' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @customer = Customer.find(params[:id])
    @request = Request.find(params[:request_id])
  end

end
