class CarsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @cars = Car.all
  end

  def style
    @style = if params[:id]     
      Car.find(params[:id]).styles
    elsif params[:make_id] and params[:model_id] and params[:year]#and params[:type] == "new"
      #year = Model.find(params[:model_id]).newest_year
      car = Car.locate(params[:year], params[:make_id], params[:model_id])
      if car
        car.styles
      else
        []
      end

    end
    
    respond_to do |format|
      #format.html # show.html.erb
      #format.xml  { render :xml => @post }
      format.json { render :json => @style }
    end 
  end

end
