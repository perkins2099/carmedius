class PagesController < HighVoltage::PagesController
  layout "home"

  def show
    if current_user and params[:id] == 'home'
      redirect_to dash_path
    else
      super
    end
  end

  protected
    def layout_for_page
      case params[:id]
      when 'home'
        'home'
      else
        'application'
      end
    end

	def redirect
		if signed_in?
	    if current_user.customer?
	      #redirect to customer root
	    elsif current_user.is_a_admin?
	      #redirect to admin root
	    end
	 	end
	end
end
