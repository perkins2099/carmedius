class BidsController < ApplicationController
    before_filter :authenticate_user!
  # GET /bids
  # GET /bids.json

  def winner
    bid = Bid.find(params[:id])
    if bid
      bid.won = true
      bid.save
      bid.request.status = 'Closed'
      bid.request.save
      DealerMailer.won_request(bid).deliver
    end
  end

  def index
    @bids = current_user.profileable.available_requests.includes([:make, :model])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bids }
    end
  end

  # GET /bids/1
  # GET /bids/1.json
  def show
    @bid = Bid.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @bid }
    end
  end

  # GET /bids/new
  # GET /bids/new.json
  def new
    @request = Request.find(params['request_id'])
    @bid = Bid.new()

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bid }
    end
  end

  # GET /bids/1/edit
  def edit
    @bid = bid.find(params[:id])
  end

  # POST /bids
  # POST /bids.json
  def create
    @bid = Bid.new(params[:bid])
    @bid.dealer_id = current_user.profileable.id
    @request = @bid.request
    respond_to do |format|
      if @bid.save
        format.html { redirect_to @bid.request, notice: 'Your bid is now live! The customer who requested the bid has been notified. Wait until they choose a winner or make another bid!' }
        format.json { render json: @bid, status: :created, location: @bid }
      else
        format.html { render action: "new" }
        format.json { render json: @bid.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /bids/1
  # PUT /bids/1.json
  def update
    @bid = bid.find(params[:id])

    respond_to do |format|
      if @bid.update_attributes(params[:bid])
        format.html { redirect_to @bid, notice: 'bid was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @bid.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bids/1
  # DELETE /bids/1.json
  def destroy
    @bid = bid.find(params[:id])
    @bid.destroy

    respond_to do |format|
      format.html { redirect_to bids_url }
      format.json { head :no_content }
    end
  end

end
