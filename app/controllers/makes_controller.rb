class MakesController < ApplicationController
  before_filter :authenticate_user!

  def index
    @makes = if params[:type] == "new"
      Make.new_makes
    elsif params[:type] == "used"
      Make.used_makes
    elsif params[:type] == "all" or params[:type].blank?
      Make.all
    end
    respond_to do |format|
      #format.html # show.html.erb
      #format.xml  { render :xml => @post }
      format.json { render :json => @makes }
    end 
  end

end
