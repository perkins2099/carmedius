class ApplicationController < ActionController::Base
  protect_from_forgery
 # Overriding the Devise current_user method
#  alias_method :devise_current_user, :current_user
  # def current_user
  #   # It will now return either a Company or a Customer, instead of the plain User.
  #   super.profileable if super
  # end

	def after_sign_in_path_for(resource)
    return dash_path
	end
end
