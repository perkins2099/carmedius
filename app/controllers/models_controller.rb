class ModelsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @models = if params[:type] == "new"
      Model.new_models
    elsif params[:type] == "used"
      Model.used_models
    elsif params[:type] == "all"
      Model.all
    end
      
    @models = if params[:make_id].present?
      @models.where(make_id: params[:make_id])
    else
      @models
    end

    respond_to do |format|
      #format.html # show.html.erb
      #format.xml  { render :xml => @post }
    format.json { render :json => @models }
    end 
  end

  def years
    model = Model.find(params[:id])
    @years = if params[:type] == "new"
      model.years.where(used: false)
    elsif params[:type] == "used"
      model.years.where(used: true)
    elsif params[:type] == "all"
      model.years
    end

    respond_to do |format|
      #format.html # show.html.erb
      #format.xml  { render :xml => @post }
    format.json { render :json => @years }
    end 
  end


end
