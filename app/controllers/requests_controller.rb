class RequestsController < ApplicationController
  before_filter :authenticate_user!
  # GET /requests
  # GET /requests.json
  def index
    if current_user.customer?
      @requests = current_user.profileable.requests.includes([:make, :model, :style, :bids, :options]).paginate(:page => params[:page])
      render "customer_index"
    elsif current_user.dealer?
      @requests = current_user.profileable.available_requests.includes([:make, :model, :style, :bids, :options]).paginate(:page => params[:page], :per_page => 20)
      render "dealer_index"
    end
  end

  # GET /requests/1
  # GET /requests/1.json
  def show
    if current_user.dealer?
      @request = Request.includes(:bids).find_by_id(params[:id])
      @bids = @request.unique_bids if @request
    elsif current_user.customer?
      @request = current_user.profileable.requests.includes(:bids).find_by_id(params[:id])
      @bids = @request.unique_bids if @request
    end


    if @request.blank?
      respond_to do |format|
        format.html { redirect_to requests_path(), notice: 'Sorry, perhaps that request removed or you do not have access to view it!' }
      end
      
    else
      @bids = [] if @bids.blank?
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @request }
      end
    end
  end

  # GET /requests/new
  # GET /requests/new.json
  def new
    @request = Request.new

    @makes = Make.new_makes
    @models = []
    @styles = []

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @request }
    end
  end

  # GET /requests/1/edit
  def edit
    @request = Request.find(params[:id])
  end

  # POST /requests
  # POST /requests.json
  def create
    @request = Request.new(params[:request])
    @request.customer_id = current_user.profileable.id
    @request.year = Model.find(params[:request][:model_id]).newest_year
    respond_to do |format|
      if @request.save
        format.html { redirect_to @request, notice: 'Request was successfully created.' }
        format.json { render json: @request, status: :created, location: @request }
      else
        format.html { render action: "new" }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /requests/1
  # PUT /requests/1.json
  def update
    @request = Request.find(params[:id])

    respond_to do |format|
      if @request.update_attributes(params[:request])
        format.html { redirect_to @request, notice: 'Request was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /requests/1
  # DELETE /requests/1.json
  def destroy
    @request = Request.find(params[:id])
    @request.destroy

    respond_to do |format|
      format.html { redirect_to requests_url }
      format.json { head :no_content }
    end
  end
end
