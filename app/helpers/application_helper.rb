module ApplicationHelper
  def bootstrap_class_for flash_type
    case flash_type
      when :success
        "alert-success"
      when :error
        "alert-error"
      when :alert
        "alert-block"
      when :notice
        "alert-info"
      else
        flash_type.to_s
    end
  end


  def d_format(tdate)
    if Time.now > (tdate+1.day)
      tdate.strftime("%-d %b")
    elsif Time.now > (tdate+1.hour)
      ((Time.now - tdate) / (60*60)).round.to_s + ' h'
    elsif Time.now > (tdate+1.minute)
      ((Time.now - tdate) / 60).round.to_s + ' m'
    else
      (Time.now - tdate).round.to_s + ' s'
    end
  end

end
